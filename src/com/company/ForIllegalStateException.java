package com.company;

import java.util.ArrayList;
        import java.util.Iterator;
        import java.util.List;

public class ForIllegalStateException {
  public static void main(String args[]) {
    try{
    List<String> arrayList = new ArrayList<String>();
    arrayList.add("a");
    arrayList.add("b");

    Iterator<String> it = arrayList.iterator();
    while (it.hasNext()) {
      it.remove();//calling remove() without calling next() throws IllegalStateException
    }
  }catch (IllegalStateException e) {
    System.out.println("Ошибка: " + e.toString());
  }}
}