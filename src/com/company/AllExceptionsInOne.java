package com.company;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Home on 02.09.2017.
 */
public class AllExceptionsInOne {
  public static void main(String[] args) {
    AllExceptionsInOne my = new AllExceptionsInOne();
    my.met1();//NullPointerException
    my.met2();//ClassCastException
    my.met3();//ClassNotFoundException
    my.met5();//IndexOutOfBoundsException
    my.met6();//NumberFormatException
    my.met7();//OutOfMemoryError
    my.met8();//StackOverflowError
    my.met9();//UnsupportedOperationException
    my.met4();//IllegalArgumentException
  }

  void met1() {
    try {
      String a = null;
      System.out.println(a.length());
    } catch (NullPointerException e) {
      System.out.println("Ошибка: " + e.toString());
    }
  }

  void met2() {
    Object ch = new String("*");
    try {
      System.out.println((Integer) ch);
    } catch (ClassCastException ex) {
      System.out.println("Ошибка: " + ex.toString());
    }
  }

  void met3() {
    String WrongClassPath = "com.company.ClassB";
    try {
      Class.forName(WrongClassPath);
      System.out.println("Class found successfully!");

    } catch (ClassNotFoundException ex) {
      System.out.println("Ошибка: " + ex.toString());

    }
  }
  void met5() {
    try {
      int[] mass = {1, 3, 5};
      System.out.println(mass[6]);
    } catch (IndexOutOfBoundsException e) {
      System.out.println("Ошибка: " + e.toString());
    }
  }

  void met6() {
    try {
      String days = "1 Sunday";
      int int1 = Integer.parseInt(days);
    } catch (NumberFormatException e) {
      System.out.println("Ошибка: " + e.toString());
    }
  }

  void met7() {
    try {
      int[] mass = new int[Integer.MAX_VALUE];
      for (int i = 0; i <= Integer.MAX_VALUE; i++) {
        mass[i] = 7;
      }
    } catch (OutOfMemoryError t) {
      System.out.println("Ошибка: " + t.toString());
    }
  }

  void met8() {
    try {
      AllExceptionsInOne smth = new AllExceptionsInOne();
      smth.culcSmth();
    } catch (StackOverflowError e) {
      System.out.println("Ошибка: " + e.toString());
    }
  }
  public int culcSmth() {
    return culcSmth();
  }

  void met9() {
    try {
      List<Integer> list1 = Arrays.asList(1, 2, 3);
      List<Integer> list2 = Arrays.asList(2, 4, 5, 6, 7, 8);
      list1.addAll(list2);
    } catch (UnsupportedOperationException e) {
      System.out.println("Ошибка: " + e.toString());
    }
  }

  void met4() {
    try {
      AllExceptionsInOne str = new AllExceptionsInOne();
      int res = str.getArea(-2, 2);
    } catch (IllegalArgumentException e) {
      System.out.println("Ошибка: " + e.toString());
      throw e;
    }
  }
  public int getArea(int a, int b) {
    int res = a * b;
    if (a < 0 || b < 0) {
      throw new IllegalArgumentException();
    }
    return res;
  }
}

