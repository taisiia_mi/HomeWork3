package com.company;

/**
 * Created by Home on 02.09.2017.
 */
public class ClassA {
  static {
    System.out.println("This is static part of ClassA");
  }
  public ClassA() {
    doSomething();
  }

  private void doSomething() {
  }
}
